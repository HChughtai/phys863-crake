-- Database: crake
-- Author: 
CREATE DATABASE crake
    WITH OWNER = postgres
        ENCODING = 'UTF8'
        TABLESPACE = pg_default
        CONNECTION LIMIT = -1;