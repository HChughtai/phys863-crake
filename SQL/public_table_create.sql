CREATE TABLE public.patients (
    patient_id serial,
    his_id integer,
    dob date,
    postcode text,
    ethnic_origin text,
    discharge_date date,
    PRIMARY KEY (patient_id)
);

CREATE INDEX ON public.patients
    (his_id);


CREATE TABLE public.his (
    his_id serial,
    his_number text,
    PRIMARY KEY (his_id)
);

CREATE TABLE public.nhs(
    nhs_id serial,
    his_id integer,
    nhs_number text,
    PRIMARY KEY (nhs_id)
);

CREATE INDEX ON public.nhs
    (nhs_id);


CREATE TABLE public.appointments (
    appointment_id serial,
    date date,
    time time without time zone,
    his_id integer,
    attend_status_id integer,
    doctor_id integer,
    missed_count integer,
    progression_score integer,
    treatment_id integer,
    surgery_probability numeric,
    PRIMARY KEY (appointment_id)
);

CREATE INDEX ON public.appointments
    (his_id);
CREATE INDEX ON public.appointments
    (attend_status_id);
CREATE INDEX ON public.appointments
    (doctor_id);
CREATE INDEX ON public.appointments
    (treatment_id);



CREATE TABLE public.treatments (
    treatment_id serial,
    description text,
    PRIMARY KEY (treatment_id)
);


CREATE TABLE public.doctors (
    doctor_id serial,
    name text,
    PRIMARY KEY (doctor_id)
);

CREATE TABLE public.surgery_info (
    surgery_info_id serial,
    agreed_date date,
    performed_date date,
    surgery_type_id integer,
    follow_up_date date,
    his_id integer,
    PRIMARY KEY (surgery_info_id)
);

CREATE INDEX ON public.surgery_info
    (surgery_type_id);
CREATE INDEX ON public.surgery_info
    (his_id);


CREATE TABLE public.surgery_type (
    surgery_type_id serial,
    description text,
    PRIMARY KEY (surgery_type_id)
);

CREATE TABLE public.attend_status (
    attend_status_id serial,
    description text,
    PRIMARY KEY (attend_status_id)
);


ALTER TABLE public.patients ADD CONSTRAINT FK_patients__his_id FOREIGN KEY (his_id) REFERENCES public.his(his_id);
ALTER TABLE public.nhs ADD CONSTRAINT FK_nhs__his_id FOREIGN KEY (his_id) REFERENCES public.his(his_id);

ALTER TABLE public.appointments ADD CONSTRAINT FK_appointments__his_id FOREIGN KEY (his_id) REFERENCES public.his(his_id);
ALTER TABLE public.appointments ADD CONSTRAINT FK_appointments__doctor_id FOREIGN KEY (doctor_id) REFERENCES public.doctors(doctor_id);
ALTER TABLE public.appointments ADD CONSTRAINT FK_appointments__treatment_id FOREIGN KEY (treatment_id) REFERENCES public.treatments(treatment_id);
ALTER TABLE public.appointments ADD CONSTRAINT FK_appointments__attend_status_id FOREIGN KEY (attend_status_id) REFERENCES public.attend_status(attend_status_id);
ALTER TABLE public.surgery_info ADD CONSTRAINT FK_surgery_info__surgery_type_id FOREIGN KEY (surgery_type_id) REFERENCES public.surgery_type(surgery_type_id);
ALTER TABLE public.surgery_info ADD CONSTRAINT FK_surgery_info__his_id FOREIGN KEY (his_id) REFERENCES public.his(his_id);